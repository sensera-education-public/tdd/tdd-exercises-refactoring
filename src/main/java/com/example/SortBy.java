package com.example;

public enum SortBy {
    NATURAL,
    NAME,
    AGE
}
