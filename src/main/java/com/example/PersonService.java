package com.example;

import java.util.ArrayList;
import java.util.List;

public class PersonService {
    List<Person> people = new ArrayList<>();

    public Person addPerson(String surname, String lastname, int age) {
        Person person = new Person(surname, lastname, age);
        people.add(person);
        return person;
    }

    public List<Person> find(String name, Integer age, SortBy sortBy) {
        return List.of();
    }
}
