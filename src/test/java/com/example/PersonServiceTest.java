package com.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PersonServiceTest {

    PersonService personService;
    private Person arne_anka;
    private Person agda_anka;
    private Person berit;
    private Person beda;
    private Person kurt;
    private Person kalle;
    private Person bert;
    private Person lisa;

    @BeforeEach
    void setUp() {
        personService = new PersonService();
        arne_anka = personService.addPerson("Arne", "Anka", 99);
        agda_anka = personService.addPerson("Agda", "Anka", 88);
        berit = personService.addPerson("Berit", "Beritsson", 109);
        beda = personService.addPerson("Beda", "Bedadotter", 88);
        kurt = personService.addPerson("Kurt", "Ohlsson", 4);
        kalle = personService.addPerson("Kalle", "Planka", 4);
        bert = personService.addPerson("Bert", "Kallesson", 2);
        lisa = personService.addPerson("Lisa", "Lisasson", 4);
    }

    @Test
    void test_find_all_persons() {
        List<Person> personList = personService.find(null, null, SortBy.NATURAL);

        assertEquals(8, personList.size());
    }

    @Test
    void test_find_all_persons_with_name() {
        List<Person> personList = personService.find("Anka", null, SortBy.NATURAL);

        assertEquals(List.of(arne_anka, agda_anka, kalle), personList);
    }

    @Test
    void test_find_all_persons_with_b_in_name_sort_by_name() {
        List<Person> personList = personService.find("b", null, SortBy.NAME);

        assertEquals(List.of(beda, berit, bert), personList);
    }

    @Test
    void test_find_all_persons_with_b_in_name_sort_by_age() {
        List<Person> personList = personService.find("b", null, SortBy.AGE);

        assertEquals(List.of(bert, beda, berit), personList);
    }

    @Test
    void test_find_all_persons_with_age_88() {
        List<Person> personList = personService.find(null, 88, SortBy.NATURAL);

        assertEquals(List.of(agda_anka, beda), personList);
    }

    @Test
    void test_find_all_persons_with_age_4_and_k_in_name_sort_by_name() {
        List<Person> personList = personService.find("k", 4, SortBy.NAME);

        assertEquals(List.of(kalle, kurt), personList);
    }
}
